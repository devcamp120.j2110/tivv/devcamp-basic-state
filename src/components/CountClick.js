import { Component } from 'react';

class CountClick extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0
        }
    }
    onBtnClick = () => {
        console.log("Ấn");
        this.setState({
            count: this.state.count + 1
        });
    }
    render() {

        return (
            <div>
            <button className='button' onClick={this.onBtnClick}>Button</button>
            <p>Số lần nhấn nút: <span>{this.state.count}</span></p>
            </div>
        )
    }
}
export default CountClick;